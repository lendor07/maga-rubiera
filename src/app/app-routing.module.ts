import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { BlogComponent } from './pages/blog/blog.component';


const routes: Routes = [
  { path: 'blog', component: BlogComponent},
  { path: 'home', component: HomeComponent },
  { path: ':servicio', component: HomeComponent },
  { path: 'contacto', component: ContactoComponent},
  { path: '', component: HomeComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ]
})
export class AppRoutingModule { }
