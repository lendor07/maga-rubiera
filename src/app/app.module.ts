import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { ModalComunicateComponent } from './components/modal-comunicate/modal-comunicate.component';
import { ModalExploraComponent } from './components/modal-explora/modal-explora.component';
import { ModalContactoComponent } from './components/modal-contacto/modal-contacto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ContactoService } from './services/contacto.service';
import { ModalAsesoriaComponent } from './components/modal-asesoria/modal-asesoria.component';
import { BlogComponent } from './pages/blog/blog.component';
import { BlogPostComponent } from './components/blog-post/blog-post.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ContactoComponent,
    ModalComunicateComponent,
    ModalExploraComponent,
    ModalContactoComponent,
    ModalAsesoriaComponent,
    BlogComponent,
    BlogPostComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ContactoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
