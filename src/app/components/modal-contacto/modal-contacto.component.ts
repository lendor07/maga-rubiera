import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import { ContactoService } from '../../services/contacto.service';

@Component({
  selector: 'app-modal-contacto',
  templateUrl: './modal-contacto.component.html',
  styleUrls: ['./modal-contacto.component.scss']
})
export class ModalContactoComponent {

  @Output() mostrar: EventEmitter<boolean>;
  @Input() visible;

  name: string;
  asunto: string;
  mail: string;
  consulta: string;
  contact = { name: '', email: '', asunto: '', consulta: '' };
  form: FormGroup;
  emailOK = false;

  constructor(private serviceContacto: ContactoService) { 
    this.mostrar = new EventEmitter();
    this.form = new FormGroup({
      name: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      asunto: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      consulta: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      email: new FormControl('', [Validators.required, Validators.pattern('^[^@]+@[^@]+\.[a-zA-Z]{2,}$')]),
    });

    this.form.setValue(this.contact, {onlySelf: true});
  }

  cerrar() {
    this.mostrar.emit(!this.visible);
  }

  enviarMail() {
    let response = this.serviceContacto.enviarMail(this.contact);

    if(response) {
this.emailOK = true;
    }
  }
}
