import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss', '../../../assets/scss/mediaQuery.scss']
})
export class NavbarComponent implements OnInit {

  @Input() visible;
  @Output() mostrarContacto: EventEmitter<boolean>;
  constructor() { this.mostrarContacto = new EventEmitter()}

  ngOnInit() {
  }

  mostrarModal() {
  this.mostrarContacto.emit(!this.visible);
  }
}
