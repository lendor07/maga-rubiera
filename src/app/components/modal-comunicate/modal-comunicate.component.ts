import { EventEmitter , Component, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-modal-comunicate',
  templateUrl: './modal-comunicate.component.html',
  styleUrls: ['./modal-comunicate.component.scss', '../../../assets/scss/mediaQuery.scss']
})
export class ModalComunicateComponent implements OnInit {

  @Output() mostrar: EventEmitter<boolean>;
  @Input() visible;
  constructor() { this.mostrar = new EventEmitter(); }

  ngOnInit() {
  }

  cerrar() {
    this.mostrar.emit(!this.visible);
  }
}
