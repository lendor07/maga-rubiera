import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-explora',
  templateUrl: './modal-explora.component.html',
  styleUrls: ['./modal-explora.component.scss', '../../../assets/scss/mediaQuery.scss']
})
export class ModalExploraComponent implements OnInit {

  @Output() mostrar: EventEmitter<any>;
  @Input() visible;

  constructor(private router: Router) { this.mostrar = new EventEmitter(); }

  ngOnInit() {
  }

  cerrar(origen) {
    let obj = []
    obj[0] = !this.visible;
    obj[1] = origen //btnCerrar o quiero mi asesoria
    this.mostrar.emit(obj);
  }

}
