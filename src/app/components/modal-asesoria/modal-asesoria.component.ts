import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-modal-asesoria',
  templateUrl: './modal-asesoria.component.html',
  styleUrls: ['./modal-asesoria.component.scss', '../../../assets/scss/mediaQuery.scss']
})
export class ModalAsesoriaComponent implements OnInit {

  @Output() mostrar: EventEmitter<boolean>;
  @Input() visible;
  constructor() { this.mostrar = new EventEmitter(); }


  ngOnInit() {
  }

  cerrar() {
    this.mostrar.emit(!this.visible);
  }
}
