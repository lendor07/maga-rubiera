import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  constructor(private http: HttpClient) { }

  enviarMail(form) {
    
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.http.post('http://magarubiera.com.ar/sendMail/contacto.php', form,{headers, responseType: 'text'}).subscribe({
      next: data => true,
      error: error => false
  })
  }
}
