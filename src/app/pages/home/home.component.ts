import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss', '../../../assets/scss/mediaQuery.scss']
})
export class HomeComponent implements OnInit {

  comunicateVisible = false;
  comunicateExplora = false;
  mostrarContacto = false;
  mostrarAsesoria = false;
  servicio = '';
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe ( params => {
      console.log(params.servicio);
      this.servicio = params.servicio;
    });

    if(this.servicio !== '') {
      console.log(this.servicio);
      this.mostrarModal(this.servicio);
    }
  }

  mostrarModal(tipo: string): void {
    switch (tipo){
      case 'asesoria':
        this.mostrarAsesoria = true;
        break;
      case 'comunicate':
        this.comunicateVisible = true;
        break;
      case 'explora':
        this.comunicateExplora = true;
        break;
      case 'contacto':
        this.mostrarContacto = true;
        break;
    }
  }
  mostrar(mostrar: any, modal: string) {
    console.log(mostrar);
    switch (modal){
      case 'asesoria':
        this.mostrarAsesoria = mostrar[0];
        break;
      case 'comunicate':
        this.comunicateVisible = mostrar[0];
        break;
      case 'explora':
        this.comunicateExplora = mostrar[0];
        if(mostrar[1] === 'asesoria') {
          this.mostrarContacto = true;
        }
        break;
      case 'contacto':
        this.mostrarContacto = mostrar;
        break;
    }
      }
}
